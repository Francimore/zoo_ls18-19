#include <iostream>

using namespace std;

class StravenkovaKarta{
private:
    string m_datumPlatnosti;
    string m_cislo;
    float m_suma;
    string m_nositel;
public:
    StravenkovaKarta(string nositel, string datumPlatnosti, string cislo){
        m_cislo=cislo;
        m_datumPlatnosti=datumPlatnosti;
        m_suma=0;
        m_nositel=nositel;
    }

    void zaplatit(float kolik){
        if(m_suma>kolik && kolik>0){
            m_suma=m_suma-kolik;
            cout << "transakce provedena" << endl;
        }else{
            cout << m_suma-kolik << endl;
        }
    }

    void nabitFinanciProstredky(float kolik){
        if(kolik>0){
            m_suma=m_suma+kolik;
        }else{
            cout<< "Zaporny pocet se nepodarilo nabit" << endl;
        }
    }

    void printInfo(){
        cout<< "karta" << endl;
        cout<< m_datumPlatnosti << endl;
        cout<< m_suma << endl;
        cout<< m_cislo << endl;
        cout<< m_nositel<< endl;
    }
};

class IS{
private:
    string m_nazev;
    float m_zakladniKapital;

    void error(){
        cout<< "nedostatek financnich prostredku" << endl;
    }
public:

    IS(string nazev){
        m_nazev=nazev;
    }

    IS(string nazev, float zakladniKapital){
        m_nazev=nazev;
        if(zakladniKapital>0) {
            m_zakladniKapital = zakladniKapital;
        }else{
            m_zakladniKapital=0;
        }
    }

    void zakazka(){
        m_zakladniKapital=m_zakladniKapital+100;
    }

    void nabitKartu(StravenkovaKarta* karta){
        if(m_zakladniKapital>=100) {
            m_zakladniKapital = m_zakladniKapital - 100;
            karta->nabitFinanciProstredky(100);
        }else{
            error();
        }
    }

    void nabitKartu(StravenkovaKarta* karta, float kolik){
        if(kolik >0 && m_zakladniKapital>kolik){
            m_zakladniKapital = m_zakladniKapital-kolik;
            karta->nabitFinanciProstredky(kolik);
        }else{
            error();
        }
    }

    void printInfo(){
        cout << "spolecnost"<< endl;
        cout<< m_nazev <<endl;
        cout<< m_zakladniKapital << endl;
    }

    ~IS(){
        cout<< "spolecnost byla zrusena" << endl;
    }

};

int main() {
    StravenkovaKarta* k1 = new StravenkovaKarta("Pepa", "11/19","32165487984132");
    StravenkovaKarta* k2 = new StravenkovaKarta("Frantisek", "11/19","32165487984132");
    StravenkovaKarta* k3 = new StravenkovaKarta("Ondrej", "11/19","32165487984132");
    IS* is = new IS("hornodolni");

    is->zakazka();
    is->nabitKartu(k2);
    k2->zaplatit(50);

    k1->printInfo();
    k2->printInfo();
    k3->printInfo();
    is->printInfo();

    delete(is);
    delete(k1);
    delete(k2);
    delete(k3);

    return 0;
}