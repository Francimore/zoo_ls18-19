#include <iostream>

#include "Sklad.h"
#include "Box.h"

int main() {

    Box* b = new Box();
    b->jmeno="MujBoxik";

    Sklad* skl= new Sklad("Brno Zemedelska 1",5);

    skl->postavPatro("Nadstavba");

    skl->ulozBox(b, 4,4);

    skl->printInfo();

    delete(skl);
    delete(b);
    return 0;
}