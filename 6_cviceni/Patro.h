//
// Created by xostrize on 26.03.2019.
//

#ifndef INC_6_CVICENI_PATRO_H
#define INC_6_CVICENI_PATRO_H

#include <iostream>
#include <vector>

#include "Box.h"

using namespace std;


class Patro {
private:
    string m_oznaceni;
    vector<Box*> m_uloznaPozice;
public:
    Patro(string oznaceni);
    void zmenOznaceni(string noveOznaceni);
    void printInfo();

    void ulozBox(Box* box,int pozice);
};


#endif //INC_6_CVICENI_PATRO_H
