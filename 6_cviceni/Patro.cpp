//
// Created by xostrize on 26.03.2019.
//

#include "Patro.h"


Patro::Patro(string oznaceni){
    m_oznaceni=oznaceni;
    for(int i=0;i<10;i++){
        m_uloznaPozice.push_back(nullptr);
    }
}
void Patro::zmenOznaceni(string noveOznaceni){
    m_oznaceni=noveOznaceni;
}
void Patro::printInfo(){
    cout<< "------------patro--------------" << endl;
    cout<< "oznaceni: \t" << m_oznaceni << endl;

    for(int i=0; i< m_uloznaPozice.size();i++){
        if(m_uloznaPozice.at(i) != nullptr){
            cout<< i <<". " << m_uloznaPozice.at(i)->jmeno << endl;
        }else{
            cout<< i <<". " << "_________________" << endl;
        }
    }


}

void Patro::ulozBox(Box* box,int pozice){
    m_uloznaPozice.at(pozice)=box;
}