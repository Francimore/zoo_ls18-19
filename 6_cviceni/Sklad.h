//
// Created by xostrize on 26.03.2019.
//

#ifndef INC_6_CVICENI_SKLAD_H
#define INC_6_CVICENI_SKLAD_H


#include <iostream>
#include <vector>
#include "Patro.h"

using namespace std;


class Sklad {
private:
    string m_adresa;
    vector<Patro*> m_patra;
public:

    Sklad(string adresa, int pocetPater);
    void printInfo();

    void postavPatro(string oznaceni);

    void ulozBox(Box* box, int patro, int pozice);

    ~Sklad();

};


#endif //INC_6_CVICENI_SKLAD_H
