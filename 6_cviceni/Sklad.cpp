//
// Created by xostrize on 26.03.2019.
//

#include "Sklad.h"


Sklad::Sklad(string adresa, int pocetPater){
    m_adresa=adresa;

    for(int i=0;i<pocetPater;i++){
        // priretezime k cislu prazdny retezec,
        // tim dojde k typove konverzi na string.
        postavPatro(to_string(i));
    }

}
void Sklad::printInfo(){
    cout<<"------------Sklad-----------" << endl;
    cout<<"adresa:" << m_adresa << endl;
    cout<<"pocet pater:" << m_patra.size() << endl;

    for(Patro* pat: m_patra){
        pat->printInfo();
    }

}

void Sklad::postavPatro(string oznaceni){
    m_patra.push_back(new Patro(oznaceni));
}

Sklad::~Sklad(){
    for(Patro* pat: m_patra){
        delete(pat);
    }
}

void Sklad::ulozBox(Box* box, int patro, int pozice){
    m_patra.at(patro)->ulozBox(box,2);
}