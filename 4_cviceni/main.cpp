#include <iostream>

using namespace std;


enum Obtiznost {
    LEVEL1, LEVEL2
};

class Jednotka {
private:
    string m_jmeno;
    int m_sila, m_obrana, m_id, m_zivot;
    static int s_id;
public:
    Jednotka(string jmeno, int sila, int obrana) {
        m_jmeno = jmeno;
        setSila(sila);
        m_id = s_id;
        s_id++;
        setObrana(obrana);
        setZivot(100);
    }

    string getJmeno() {
        return m_jmeno;
    }

    int getSila() {
        return m_sila;
    }

    int getObrana() {
        return m_obrana;
    }

    int getZivot() {
        return m_zivot;
    }

    int getId() {
        return m_id;
    }

    void setZivot(int zivot) {
        if (zivot > 100 || zivot < 0) {
            cout << "zivot se pohybuje v rozmezi 0-100" << endl;
            m_zivot = 100;
        } else {
            m_zivot = zivot;
        }
    }

    void odeberZivoty(int kolik) {
        if ((m_zivot - kolik) > 0) {
            m_zivot = m_zivot - kolik;
        } else {
            m_zivot = 0;
        }
    }

    void setSila(int sila) {
        m_sila = sila;
    }

    void setObrana(int obrana) {
        m_obrana = obrana;
    }

    void printInfo() {
        cout << "-------------Jednotka------------" << endl;
        cout << "Jmeno jednotky: \t" << getJmeno() << endl;
        cout << "Sila Jednotky: \t\t" << getSila() << endl;
        cout << "Obrana jednotky: \t" << getObrana() << endl;
        cout << "Zivoty Jednotky: \t" << getZivot() << endl;
        cout << "Id Jednotky: \t\t" << getId() << endl;
        cout << "---------------------------------" << endl;
        cout << "pocet vytvorenych jedotek:" << Jednotka::getPocetVytvorenychJednotek() << endl;
    }

    static int getPocetVytvorenychJednotek() {
        return s_id;
    }

    static Jednotka *getJednotka(Obtiznost level) {
        switch (level) {
            case LEVEL1:
                return new Jednotka("Pretoriani", 10, 20);
            case LEVEL2:
                return new Jednotka("Pretoriani", 20, 30);
            default:
                return new Jednotka("Pretoriani", 10, 20);
        }
    }


    void boj(Jednotka *sKym) {
        if (getObrana() > sKym->getSila()) {
            cout << getJmeno() << " se ubranila" << endl;
        } else {
            int rozdil = sKym->getSila() - getObrana();
            cout << getJmeno() << "byli zraneni o:" << rozdil << endl;
            odeberZivoty(rozdil);
        }
    }
};

int Jednotka::s_id = 0;


int main() {

    Jednotka *pesi = new Jednotka("Pretoriani", 10, 20);
    pesi->printInfo();

    Jednotka *jizdni = new Jednotka("Jizdni Jednotka", 30, 20);
    jizdni->printInfo();

    pesi->boj(jizdni);

    pesi->printInfo();


    Jednotka *genPesi = Jednotka::getJednotka(Obtiznost::LEVEL2);
    genPesi->printInfo();
    delete (genPesi);

    delete (jizdni);
    delete (pesi);


    return 0;
}