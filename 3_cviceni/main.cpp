#include <iostream>
#include <vector>

using namespace std;

class Univerzita{
private:
    string m_jmeno;
    string m_sidlo;
    int m_pocetPublikaci;
    vector<string> m_studenti;

    void imatrikulace(){
        cout<< "Studenta jsme zapsali ke studiu" << endl;
    }

    string prectiVstup(){
        string vstup="";
        cout<<"zadej novy vstup:" << endl;
        cin >> vstup;
        return vstup;
    }

public:

    Univerzita(string jmeno, string sidlo){
        setJmeno(jmeno);
        setSidlo(sidlo);
        m_pocetPublikaci=0;
    }

    void setJmeno(string jmeno){
        if(jmeno==""){
            cout<< "jmeno se nam nelibi" << endl;
            setJmeno(prectiVstup());
        }else {
            m_jmeno = jmeno;
        }
    }

    void setSidlo(string sidlo){
        if(sidlo==""){
            m_sidlo="Nezname";
        }else {
            m_sidlo = sidlo;
        }
    }

    void zapisStudenta(string jmeno, string rodneCislo){
        string student = jmeno + " " + rodneCislo;
        m_studenti.push_back(student);
        imatrikulace();
    }

    int getPocetStudentu(){
        return m_studenti.size();
    }

    void printInfo(){
        cout<< "----------Univerzita--------------"<< endl;
        cout<< "Nazev: \t\t" << m_jmeno << endl;
        cout<< "Sidlo: \t\t" << m_sidlo << endl;
        cout<< "Pocet studentu: " << getPocetStudentu() << endl;
        for(string student: m_studenti){
            cout<< "Student: " << student << endl;
        }


        cout<< "Pocet publikaci: " << m_pocetPublikaci << endl;

    }
};

int main() {
    string nazev;
    cout<< "zadej nazev univerzity" << endl;
    cin>>nazev;
    Univerzita* univ = new Univerzita(nazev, "Zemedelska 1, 613 00");
    univ->printInfo();
    univ->zapisStudenta("krtecek","123456789");
    univ->zapisStudenta("mys","987654321");
    univ->printInfo();
    univ->setJmeno("VUT");
    univ->setSidlo("technicka 13, 615 00");
    univ->printInfo();

    delete(univ);
    return 0;
}