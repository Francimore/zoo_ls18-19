cmake_minimum_required(VERSION 3.12)
project(9_cviceni)

set(CMAKE_CXX_STANDARD 14)

add_executable(9_cviceni main.cpp Obluda.cpp Obluda.h KontejnerOblud.cpp KontejnerOblud.h)