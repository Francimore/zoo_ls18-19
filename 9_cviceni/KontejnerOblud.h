//
// Created by xostrize on 16.04.2019.
//

#ifndef INC_9_CVICENI_KONTEJNEROBLUD_H
#define INC_9_CVICENI_KONTEJNEROBLUD_H

#include <vector>
#include "Obluda.h"

using namespace std;

class KontejnerOblud {
public:
    vector<Obluda*> obludy;
};


#endif //INC_9_CVICENI_KONTEJNEROBLUD_H
