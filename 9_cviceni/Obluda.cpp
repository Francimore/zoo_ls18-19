//
// Created by xostrize on 16.04.2019.
//

#include "Obluda.h"

#include "KontejnerOblud.h"


int Obluda::id=0;


Obluda::Obluda(){

    m_obrana=10;
    m_utok=10;
    m_zivot=100;
    m_jmeno="";
    rodneCislo=generujId();
    cout<< "tady vznika zivocich "<< id << endl;
}

Obluda::Obluda(string jmeno, int zivot, int obrana, int utok){
    cout<< "tady vznika " << jmeno << endl;
    m_jmeno=jmeno;
    m_obrana=zivot;
    m_utok=obrana;
    m_zivot=utok;
    rodneCislo=generujId();
}

Obluda* Obluda::getObluda(){
    return new Obluda();
}

void Obluda::printInfo(){
    cout<<"obluda -----------------------" << endl;
    cout<<"jmeno " << m_jmeno << endl;
    cout<<"zivoty " << m_zivot << endl;
    cout<<"obrana" << m_obrana << endl;
    cout<<"utok" << m_utok<< endl;
    cout<<"rodneCislo "<< rodneCislo << endl;
}


Obluda::~Obluda(){
    if(m_jmeno=="") {
        cout << rodneCislo<< " umira" << endl;
    }else{
        cout << m_jmeno<<" umira" << endl;
    }
}

KontejnerOblud* Obluda::farmareni(int produkce) {
    KontejnerOblud* kontejner = new KontejnerOblud();

    for(int i=0; i<produkce; i++) {
        kontejner->obludy.push_back(Obluda::getObluda());
    }

    return kontejner;
}


void Obluda::zpracujKontejner(KontejnerOblud* kontejner){
    cout<< "vidim tu " << kontejner->obludy.size() << " nadejnych kusu" << endl;
    for(Obluda* zvire: kontejner->obludy){
        delete(zvire);
        kontejner->obludy.pop_back();
    }
    delete(kontejner);
}

int Obluda::generujId() {
    id++;
    return id;
}
