#include <iostream>

#include "Obluda.h"
#include "KontejnerOblud.h"

int main() {
    Obluda* ob  = new Obluda("Frantisek", 100,10,10);
    Obluda* ob2 = new Obluda("Pepa", 100,10,10);

    KontejnerOblud* zasoby= ob->farmareni(10);
    ob2->zpracujKontejner(zasoby);

    delete(ob2);
    delete(ob);


    return 0;
}