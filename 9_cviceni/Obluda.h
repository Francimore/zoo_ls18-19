//
// Created by xostrize on 16.04.2019.
//

#ifndef INC_9_CVICENI_OBLUDA_H
#define INC_9_CVICENI_OBLUDA_H

#include <iostream>

class KontejnerOblud;

using namespace std;

class Obluda {
private:
    int m_zivot;
    int m_obrana;
    int m_utok;
    string m_jmeno;
    int rodneCislo;

public:
    static int id;


    Obluda();
    Obluda(string m_jmeno, int zivot, int obrana, int utok);

    static Obluda* getObluda();

    static int generujId();

    KontejnerOblud* farmareni(int produkce);

    void zpracujKontejner(KontejnerOblud* kontejner);

    void printInfo();
    ~Obluda();

};



#endif //INC_9_CVICENI_OBLUDA_H
