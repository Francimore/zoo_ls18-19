#include <iostream>

using namespace std;

class Auto {
public:
    string m_spz = "";
    float m_objem = 0;
    int m_pocetMistKSezeni = 0;
    int m_pocetRychlostnichStupnu = 0;
    int m_aktualneZarazenyStupen = 0;
    string m_barva = "";
    int m_maxRychlost = 0;
    int m_pocetLakovani = 0;

    void zaradRychlost(int rychlost){
        m_aktualneZarazenyStupen = rychlost;
    }

    string getBarva(){
        return m_barva;
    }

    void setBarva(string barva){
        m_barva = barva;
        m_pocetLakovani++;
    }

    void printInfo(){
        m_spz = "brno007";
        m_barva = "cervena";

        zaradRychlost(5);

        cout<< "zarazena rychlost je: \t" << m_aktualneZarazenyStupen << endl;

        cout<< "barva auta je: \t" << getBarva() << endl;

        setBarva("Ruzova");
        cout<< "barva auta je: \t" << getBarva()<< endl;
        cout<< "pocet Lakovani: \t" << m_pocetLakovani << endl;

        std::cout << "Hello, World!" << std::endl;
        cout << m_spz << std::endl;

    }

};


int main() {

    Auto *ferrari = new Auto();

    ferrari->printInfo();

    delete ferrari;
    return 0;
}