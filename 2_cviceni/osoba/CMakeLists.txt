cmake_minimum_required(VERSION 3.12)
project(osoba)

set(CMAKE_CXX_STANDARD 14)

add_executable(osoba main.cpp)