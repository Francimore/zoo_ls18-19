#include <iostream>

using namespace std;

class Clovek {
public:
    int m_vek;
    float m_vyska;
    float m_vaha;
    bool m_pohlavi;
    string m_jmeno;
    string m_prijmeni;
    string m_rodneCislo;

    Clovek() {
        m_jmeno = "pepa";
        m_prijmeni = "z depa";
        m_pohlavi = false;
        m_vaha = 60;
        m_vyska = 1.8;
        m_rodneCislo = "3265459/6547";
        m_vek = 40;
    }

    Clovek(string jmeno, string prijmeni, string rodneCislo, float vyska, float vaha, bool pohlavi, int vek) {
        m_jmeno = jmeno;
        m_prijmeni = prijmeni;
        m_rodneCislo = rodneCislo;
        m_vek = vek;
        setVyska(vyska);
        setVaha(vaha);
        m_pohlavi = pohlavi;
    }

    ~Clovek() {
        cout << "Clovek umira " << getCeleJmeno() << " ve veku " << getVek() << " roku." << endl;
    }

    void setVyska(float vyska) {
        if (vyska > 0) {
            m_vyska = vyska;
        } else {
            m_vyska = 0;
            cout << "zaporne cislo nemuze byt" << endl;
            cout << "preskakuji vstup" << endl;
        }

    }

    void setVaha(float vaha) {
        if (vaha > 0) {
            m_vaha = vaha;
        } else {
            m_vaha = 0;
            cout << "zaporne cislo nemuze byt" << endl;
            cout << "preskakuji vstup" << endl;
        }
    }

    string getCeleJmeno() {
        return m_jmeno + " " + m_prijmeni;
    }

    string getJmeno() {
        return m_jmeno;
    }

    string getPrijmeni() {
        return m_prijmeni;
    }

    float getVyska() {
        return m_vyska;
    }

    float getVaha() {
        return m_vaha;
    }

    float getBMIindex() {
        return (m_vaha / (m_vyska * m_vyska));
    }

    bool getPohlavi() {
        return m_pohlavi;
    }

    int getVek() {
        return m_vek;
    }

    string prelozPohlavi(bool pohlavi) {
        if (pohlavi) {
            return "muz";
        } else {
            return "zena";
        }
    }
/* víceřádkový komentář
    string getRodneCislo() {
        return m_rodneCislo;
    }
*/
    string getRodneCislo() {
        return m_rodneCislo;
    }

    /**
 * tohle je priklad dokumentace
 * tahle funkce dělá, připisuje vrací atp... @param, @ret, @see atp... <a href="www.google.com"> na tom to odkazu </a>
 */
    void narozeniny() {
	//tyhle dva následující příkazy jsou ekvivalentní
        m_vek++;
        //m_vek=m_vek+1;
	//jednořádkový komentář

    }

    void printInfo() {
        cout << "-------Clovek-------" << endl;
        cout << "Jmeno: \t" << getCeleJmeno() << endl;
        cout << "Rodne cislo: \t" << getRodneCislo() << endl;
        cout << "Vek: \t" << getVek() << endl;
        cout << "Pohlavi:\t" << prelozPohlavi(getPohlavi()) << endl;
        cout << "BMI index: \t" << getBMIindex() << endl;
        cout << "--------------------" << endl;
    }
};

int main() {

    Clovek *pepa = new Clovek();

    pepa->printInfo();
    delete pepa;


    cout << "Zvol si svuj jmeno:" << endl;
    string jmeno = "";
    cin >> jmeno;

    Clovek *frantisek = new Clovek(jmeno, "Ostrizek", "xxxxxxx/yyyy", 2.1, 100, true, 27);

    frantisek->printInfo();

    cout << frantisek->prelozPohlavi(true) << endl;
    frantisek->narozeniny();

    cout<< frantisek->getVek() << endl;
    delete frantisek;
    return 0;
}
