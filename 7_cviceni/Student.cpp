//
// Created by xmuron1 on 08.04.2019.
//

#include "Student.h"


Student::Student(std::string jmeno, int stipendium) : Osoba(jmeno){
    m_stipendium = stipendium;
}


int Student::getStipendium() {
    return m_stipendium;
}
