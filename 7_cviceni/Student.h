//
// Created by xmuron1 on 08.04.2019.
//

#ifndef UNTITLED_STUDENT_H
#define UNTITLED_STUDENT_H

#include <iostream>
#include "Osoba.h"

class Student : public Osoba {
private:
    int m_stipendium;
public:
    Student(std::string jmeno, int stipendium);
    int getStipendium();
};


#endif //UNTITLED_STUDENT_H
