//
// Created by xmuron1 on 08.04.2019.
//

#ifndef UNTITLED_IS_H
#define UNTITLED_IS_H

#include <iostream>
#include "Student.h"
#include "Ucitel.h"
#include <vector>

class IS {
private:
    std::string m_jmenoSkoly;
    std::vector<Student*> m_seznamStudentu;
    std::vector<Ucitel*> m_seznamUcitelu;
public:
    IS(std::string jmenoSkoly);
    void pridejStudenta(Student* novyStudent);
    void pridejUcitele(Ucitel* novyUcitel);
    void vypisStudenty();
    void vypisUcitele();
    int getSumaStipendii();
};


#endif //UNTITLED_IS_H
