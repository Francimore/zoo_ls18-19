//
// Created by xmuron1 on 08.04.2019.
//

#ifndef UNTITLED_OSOBA_H
#define UNTITLED_OSOBA_H

#include <iostream>
class Osoba {
private:
    std::string m_jmeno;
public:
    Osoba(std::string jmeno);
    std::string getJmeno();
};


#endif //UNTITLED_OSOBA_H
